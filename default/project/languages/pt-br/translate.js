module.exports = {
  wellcome: 'Seja bem vindo(a)!',
  forgot_password: 'Perdeu a senha?',
  log_in: 'Entrar',
  forgot_your_password: 'Clique para recuperar senha',
  home: 'Inicio',
  continue: 'Continuar',
  need_an_account: 'Precisa de criar uma conta?',
  register_here: 'Crie aqui.'
}
