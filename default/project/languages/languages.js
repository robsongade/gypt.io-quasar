module.exports = {
  language_default: {
    name: 'English',
    code: 'en',
    icon: 'en.png'
  },
  languages: [{
    name: 'English',
    code: 'en',
    icon: 'https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/0.8.2/flags/4x3/us.svg'
  }, {
    name: 'Portugues Brasil',
    code: 'pt-br',
    icon: require('./pt-br/icon.png')
  }],
  modules: [{
    code: 'install',
    folder: '/'
  }, {
    code: 'translate',
    folder: '/'
  }]
}
