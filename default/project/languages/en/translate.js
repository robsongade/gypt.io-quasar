module.exports = {
  wellcome: 'Wellcome!',
  forgot_password: 'Forgot your password?',
  log_in: 'LogIn',
  forgot_your_password: 'Forgot your password',
  home: 'Home',
  continue: 'Continue',
  need_an_account: 'Need an account?',
  register_here: 'Register here.'
}
